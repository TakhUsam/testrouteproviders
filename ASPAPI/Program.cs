using ASPAPI.Models;
using ASPAPI.Models.MappingProfiles;
using ASPAPI.Services.Implementation;
using ASPAPI.Services.Implementations.Providers;
using ASPAPI.Services.Interfaces;
using ASPAPI.Utils;
using Microsoft.Extensions.Caching.Memory;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddHttpClient<ProviderOneService>();
builder.Services.AddHttpClient<ProviderTwoService>();

builder.Services.AddAutoMapper(typeof(MapProfiles));

builder.Services.AddSingleton<ICache<SearchRequest, SearchResponse>, MemoryCache<SearchRequest, SearchResponse>>(serviceProvider =>
    //�������� �� ������� ����� �������� ����� ���������� �� �������� ����� ��������� ����
    //�� ������ ��� �� ������ ������ ��� ��������, � ������� ����� ���������������
    new MemoryCache<SearchRequest, SearchResponse>(null, (request, response) => DateTime.Now - request.OriginDateTime.AddMinutes(-response.MaxMinutesRoute))
);

builder.Services.AddTransient<IProviderService<ProviderOneSearchRequest, ProviderOneSearchResponse>, ProviderOneService>();
builder.Services.AddTransient<IProviderService<ProviderTwoSearchRequest, ProviderTwoSearchResponse>, ProviderTwoService>();

builder.Services.AddScoped<ISearchService, SearchService>();
builder.Services.AddScoped<ICacheSearchService, CachedSearchService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.Run();


