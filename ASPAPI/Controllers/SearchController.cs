﻿using ASPAPI.Models;
using ASPAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace ASPAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SearchController : ControllerBase
    {
        private readonly ISearchService _searchService;
        private readonly ICacheSearchService _cacheSearchService;

        public SearchController(ISearchService searchService, ICacheSearchService cacheSearchService)
        {
            _searchService = searchService;
            _cacheSearchService = cacheSearchService;
        }

        [HttpPost]
        public async Task<IActionResult> SearchRoutes([FromBody] SearchRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var response = await _cacheSearchService.SearchAsync(request);
            return Ok(response);
        }

        [HttpGet("ping")]
        public async Task<IActionResult> Ping()
        {
            return Ok(await _searchService.CheckAvailabilityAsync());
        }
    }
}