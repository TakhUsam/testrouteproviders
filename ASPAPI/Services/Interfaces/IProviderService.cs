﻿namespace ASPAPI.Services.Interfaces
{
    public interface IProviderService<TRuquest, TResult>
    {
        Task<bool> CheckAvailabilityAsync();
        Task<TResult> SearchAsync(TRuquest request);
    }
}