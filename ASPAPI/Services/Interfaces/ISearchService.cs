﻿using ASPAPI.Models;

namespace ASPAPI.Services.Interfaces
{
    public interface ISearchService
    {
        Task<SearchResponse> SearchAsync(SearchRequest request);
        Task<bool> CheckAvailabilityAsync();
    }
}