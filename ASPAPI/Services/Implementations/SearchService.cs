﻿
using AutoMapper;
using ASPAPI.Models;
using ASPAPI.Services.Interfaces;

namespace ASPAPI.Services.Implementation
{
    public class SearchService : ISearchService
    {
        private readonly IProviderService<ProviderOneSearchRequest, ProviderOneSearchResponse> _providerOneService;
        private readonly IProviderService<ProviderTwoSearchRequest, ProviderTwoSearchResponse> _providerTwoService;
        private readonly IMapper _mapper;
        private readonly ILogger<SearchService> _logger;

        public SearchService(
            IProviderService<ProviderOneSearchRequest, ProviderOneSearchResponse> providerOneService,
            IProviderService<ProviderTwoSearchRequest, ProviderTwoSearchResponse> providerTwoService,
            IMapper mapper,
            ILogger<SearchService> logger)
        {
            _providerOneService = providerOneService;
            _providerTwoService = providerTwoService;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<SearchResponse> SearchAsync(SearchRequest request)
        {

            var providerOneTask = SearchProviderAsync<ProviderOneSearchRequest, ProviderOneSearchResponse>(_providerOneService, request);
            var providerTwoTask = SearchProviderAsync<ProviderTwoSearchRequest, ProviderTwoSearchResponse>(_providerTwoService, request);

            var responses = await Task.WhenAll(providerOneTask, providerTwoTask);

            return responses.Aggregate((a, b) =>
            {
                a.Routes.Concat(b.Routes);
                a.MinMinutesRoute = Math.Min(a.MinMinutesRoute, b.MinMinutesRoute);
                a.MaxMinutesRoute = Math.Max(a.MaxMinutesRoute, b.MaxMinutesRoute);
                a.MinPrice = Math.Min(a.MinPrice, b.MinPrice);
                a.MaxPrice = Math.Max(a.MaxPrice, b.MaxPrice);
                return a;
            });
        }

        public async Task<bool> CheckAvailabilityAsync()
        {
            var checkServices = await Task.WhenAll(_providerOneService.CheckAvailabilityAsync(), _providerTwoService.CheckAvailabilityAsync());
            //наверное если хотя бы один провайдер работает то мы тоже работаем
            return checkServices.Any(x => x);
        }

        private async Task<SearchResponse> SearchProviderAsync<TRequest, TResponse>(IProviderService<TRequest, TResponse> providerService, SearchRequest searchRequest)
        {
            try
            {
                //Технически является лишним запросом, но по ТЗ кажется надо было проверять доступность
                if (await providerService.CheckAvailabilityAsync())
                {
                    var providerResponse = await providerService.SearchAsync(_mapper.Map<TRequest>(searchRequest));
                    return _mapper.Map<SearchResponse>(providerResponse);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when polling provider " + nameof(providerService));
            }

            return new SearchResponse();
        }
    }
}
