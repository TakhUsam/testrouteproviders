﻿using ASPAPI.Models;
using ASPAPI.Services.Interfaces;
using Newtonsoft.Json;

namespace ASPAPI.Services.Implementations.Providers
{
    public class ProviderTwoService : ProviderService<ProviderTwoSearchRequest, ProviderTwoSearchResponse>
    {
        protected override string ProviderApi => "http://provider-two/api/v1";

        public ProviderTwoService(HttpClient httpClient) : base(httpClient) { }
    }
}