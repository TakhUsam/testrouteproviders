﻿using ASPAPI.Services.Interfaces;
using Newtonsoft.Json;

namespace ASPAPI.Services.Implementations.Providers
{
    public abstract class ProviderService<TProviderRequest, TProviderResponse> : IProviderService<TProviderRequest, TProviderResponse>
    {
        private readonly HttpClient _httpClient;
        //немного шаблонного "метода"
        protected abstract string ProviderApi { get; }

        public ProviderService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<bool> CheckAvailabilityAsync()
        {
            var response = await _httpClient.GetAsync($"{ProviderApi}/ping");
            return response.IsSuccessStatusCode;
        }

        public async Task<TProviderResponse> SearchAsync(TProviderRequest request)
        {
            var jsonRequest = JsonConvert.SerializeObject(request);
            var content = new StringContent(jsonRequest, System.Text.Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync($"{ProviderApi}/search", content);

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                var searchResponse = JsonConvert.DeserializeObject<TProviderResponse>(jsonResponse);
                return searchResponse;
            }

            throw new Exception("Failed to retrieve data from Provider One");
        }
    }
}