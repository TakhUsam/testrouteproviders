﻿using ASPAPI.Models;
using ASPAPI.Services.Implementations.Providers;
using ASPAPI.Services.Interfaces;
using Newtonsoft.Json;

namespace ASPAPI.Services.Implementation
{
    public class ProviderOneService : ProviderService<ProviderOneSearchRequest, ProviderOneSearchResponse>
    {
        protected override string ProviderApi => "http://provider-one/api/v1";

        public ProviderOneService(HttpClient httpClient): base(httpClient) { }
    }
}