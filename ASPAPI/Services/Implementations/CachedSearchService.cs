﻿
using ASPAPI.Models;
using ASPAPI.Services.Interfaces;
using ASPAPI.Utils;


namespace ASPAPI.Services.Implementation
{
    public class CachedSearchService : ICacheSearchService
    {
        private readonly ISearchService _searchService;
        private readonly ICache<SearchRequest, SearchResponse> _cacheService;

        public CachedSearchService(ISearchService searchService, ICache<SearchRequest, SearchResponse> cacheService)
        {
            _searchService = searchService;
            _cacheService = cacheService;
        }

        public async Task<SearchResponse> SearchAsync(SearchRequest request)
        {
            if (request.Filters?.OnlyCached ?? false)
            {
                return await _cacheService.GetOrAddAsync(request, () => _searchService.SearchAsync(request));
            }
            else
            {
                var response = await _searchService.SearchAsync(request);
                _ = _cacheService.GetOrAddAsync(request, () => Task.FromResult(response));
                return response;
            }
        }

        public async Task<bool> CheckAvailabilityAsync()
        {
            return await _searchService.CheckAvailabilityAsync();
        }
    }
}