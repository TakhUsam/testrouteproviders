﻿using Microsoft.Extensions.Caching.Memory;

namespace ASPAPI.Utils
{
    public class MemoryCache<TRequest, TResult> : ICache<TRequest, TResult>
    {
        private readonly MemoryCache _cache;
        private readonly TimeSpan _defaultExpiryTime = new TimeSpan(0, 10, 0);
        private readonly Func<TRequest, TResult, TimeSpan>? _expiryTimeFunc;

        public MemoryCache(MemoryCacheOptions? memoryCacheOptions, Func<TRequest, TResult, TimeSpan>? expiryTimeFunc)
        {
            _cache = new MemoryCache(memoryCacheOptions ?? new MemoryCacheOptions());
            _expiryTimeFunc = expiryTimeFunc;
        }

        public async Task<TResult> GetOrAddAsync(TRequest request, Func<Task<TResult>> valueFactory)
        {
            var cacheKey = GetCacheKey(request);
            if (_cache.Get(cacheKey) is TResult result)
            {
                return result;
            }

            result = await valueFactory();

            var expireTime = _expiryTimeFunc == null ? _defaultExpiryTime : _expiryTimeFunc(request, result);

            _cache.Set(cacheKey, result, expireTime);

            return result;
        }

        private int GetCacheKey(TRequest request)
        {
            return request.GetHashCode();
        }
    }
}