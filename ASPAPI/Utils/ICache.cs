﻿namespace ASPAPI.Utils
{
    public interface ICache<TRequest, TResult>
    {
        Task<TResult> GetOrAddAsync(TRequest request, Func<Task<TResult>> valueFactory);
    }
}