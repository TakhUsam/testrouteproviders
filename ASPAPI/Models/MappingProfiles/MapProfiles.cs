﻿using AutoMapper;

namespace ASPAPI.Models.MappingProfiles
{
    public class MapProfiles : Profile
    {
        public MapProfiles()
        {
            //Мапы не уверен что корректны, просто как пример
            CreateMap<ProviderOneSearchResponse, SearchResponse>()
                .ForMember(dest => dest.Routes, opt => opt.MapFrom(src => src.Routes))
                .ForMember(dest => dest.MinPrice, opt => opt.MapFrom(src => src.Routes.Min(r => r.Price)))
                .ForMember(dest => dest.MaxPrice, opt => opt.MapFrom(src => src.Routes.Max(r => r.Price)))
                .ForMember(dest => dest.MinMinutesRoute, opt => opt.MapFrom(src => src.Routes.Min(r => (int)(r.TimeLimit - r.DateFrom).TotalMinutes)))
                .ForMember(dest => dest.MaxMinutesRoute, opt => opt.MapFrom(src => src.Routes.Max(r => (int)(r.TimeLimit - r.DateFrom).TotalMinutes)));

            CreateMap<ProviderTwoSearchResponse, SearchResponse>()
                .ForMember(dest => dest.Routes, opt => opt.MapFrom(src => src.Routes))
                .ForMember(dest => dest.MinPrice, opt => opt.MapFrom(src => src.Routes.Min(r => r.Price)))
                .ForMember(dest => dest.MaxPrice, opt => opt.MapFrom(src => src.Routes.Max(r => r.Price)))
                .ForMember(dest => dest.MinMinutesRoute, opt => opt.MapFrom(src => src.Routes.Min(r => (int)(r.TimeLimit - r.Departure.Date).TotalMinutes)))
                .ForMember(dest => dest.MaxMinutesRoute, opt => opt.MapFrom(src => src.Routes.Max(r => (int)(r.TimeLimit - r.Departure.Date).TotalMinutes)));

            CreateMap<ProviderOneRoute, Route>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Origin, opt => opt.MapFrom(src => src.From))
                .ForMember(dest => dest.Destination, opt => opt.MapFrom(src => src.To))
                .ForMember(dest => dest.OriginDateTime, opt => opt.MapFrom(src => src.DateFrom))
                .ForMember(dest => dest.DestinationDateTime, opt => opt.MapFrom(src => src.DateTo));


            CreateMap<ProviderTwoRoute, Route>()
                .ForMember(dest => dest.Id, opt => opt.Ignore())
                .ForMember(dest => dest.Origin, opt => opt.MapFrom(src => src.Departure.Point))
                .ForMember(dest => dest.Destination, opt => opt.MapFrom(src => src.Arrival.Point))
                .ForMember(dest => dest.OriginDateTime, opt => opt.MapFrom(src => src.Departure.Date))
                .ForMember(dest => dest.DestinationDateTime, opt => opt.MapFrom(src => src.Arrival.Date));

            CreateMap<SearchRequest, ProviderOneSearchRequest>()
                .ForMember(dest => dest.From, opt => opt.MapFrom(src => src.Origin))
                .ForMember(dest => dest.To, opt => opt.MapFrom(src => src.Destination))
                .ForMember(dest => dest.DateFrom, opt => opt.MapFrom(src => src.OriginDateTime))
                .ForMember(dest => dest.DateTo, opt => opt.MapFrom(src => src.Filters == null ? null : src.Filters.DestinationDateTime))
                .ForMember(dest => dest.MaxPrice, opt => opt.MapFrom(src => src.Filters == null ? null : src.Filters.MaxPrice));


            CreateMap<SearchRequest, ProviderTwoSearchRequest>()
               .ForMember(dest => dest.Departure, opt => opt.MapFrom(src => src.Origin))
               .ForMember(dest => dest.Arrival, opt => opt.MapFrom(src => src.Destination))
               .ForMember(dest => dest.DepartureDate, opt => opt.MapFrom(src => src.OriginDateTime))
               .ForMember(dest => dest.MinTimeLimit, opt => opt.MapFrom(src => src.Filters == null ? null : src.Filters.MinTimeLimit));

        }
    }
}