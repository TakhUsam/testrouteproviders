﻿namespace ASPAPI.Models
{
    public class SearchRequest
    {
        // Mandatory
        // Start point of route, e.g. Moscow 
        public string Origin { get; set; }

        // Mandatory
        // End point of route, e.g. Sochi
        public string Destination { get; set; }

        // Mandatory
        // Start date of route
        public DateTime OriginDateTime { get; set; }

        // Optional
        public SearchFilters? Filters { get; set; }

        public override int GetHashCode()
        {
            //TODO: Надо переопределить т.к. хэш классов связан с адресом в памяти а не значением
            return (Origin, Destination, OriginDateTime).GetHashCode();
        }
    }
}